importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');


const firebaseConfig = {
  apiKey: "AIzaSyDZFAg5yNjdvtickWXfQiSBQRtQSPXwu_s",
  authDomain: "hackingforhumanity-fd631.firebaseapp.com",
  databaseURL:
    "https://hackingforhumanity-fd631-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "hackingforhumanity-fd631",
  storageBucket: "hackingforhumanity-fd631.appspot.com",
  messagingSenderId: "894299534254",
  appId: "1:894299534254:web:e683a7aafc0154b9188e3f"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
  console.log('Received background message ', payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});