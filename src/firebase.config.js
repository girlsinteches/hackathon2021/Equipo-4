// Import the functions you need from the SDKs you need
import firebase from "firebase";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDZFAg5yNjdvtickWXfQiSBQRtQSPXwu_s",
  authDomain: "hackingforhumanity-fd631.firebaseapp.com",
  databaseURL:
    "https://hackingforhumanity-fd631-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "hackingforhumanity-fd631",
  storageBucket: "hackingforhumanity-fd631.appspot.com",
  messagingSenderId: "894299534254",
  appId: "1:894299534254:web:e683a7aafc0154b9188e3f"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var database = firebase.database();
var messaging = firebase.messaging();

export {database, messaging};
