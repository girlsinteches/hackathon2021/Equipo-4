import { useState } from "react"
import { Link } from "react-router-dom"
import { Box, TextField } from "@mui/material"
import Arrow from '@mui/icons-material/ArrowForward';
import ChevronLeft from '@mui/icons-material/ChevronLeft';

function Duration() {
  const [duration, setDuration] = useState(0)
  return (
    <Box className="main-container">
        <Box>
        <Link className="return-button" variant="text" to="/week-day" >
          <ChevronLeft fontSize="medium"/> <span>Volver</span>
        </Link>
          <p className="text-a">¿Cuántas semanas durará el ciclo?</p>
          <p>Indica cuántas sesiones serán</p>
          <TextField
            fullWidth
            type="number"
            initialValue={duration}
            onChange={setDuration}
          />
        </Box>
        <Link className="main-button" to="/first-session">
          Siguiente <Arrow fontSize="large"/>
        </Link>
      </Box>
    )
}

export { Duration }