import { useState } from "react"
import { Link } from "react-router-dom"
import { Box, MenuItem, Select } from "@mui/material"
import Arrow from '@mui/icons-material/ArrowForward';
import ChevronLeft from '@mui/icons-material/ChevronLeft';

function WeekDay() {
  const [weekDay, setWeekDay] = useState()
  return (
    <Box className="main-container">
        <Box>
        <Link className="return-button" variant="text" to="/name" >
          <ChevronLeft fontSize="medium"/> <span>Volver</span>
        </Link>
          <p className="text-a">¿Qué día de la semana serán las sesiones?</p>
          <Select
            fullWidth
            value={weekDay}
            onChange={setWeekDay}
          >
            <MenuItem value="lunes">Lunes</MenuItem>
            <MenuItem value="martes">Martes</MenuItem>
            <MenuItem value="miercoles">Miércoles</MenuItem>
            <MenuItem value="jueves">Jueves</MenuItem>
            <MenuItem value="viernes">Viernes</MenuItem>
            <MenuItem value="sabado">Sábado</MenuItem>
            <MenuItem value="domingo">Domingo</MenuItem>
          </Select>
        </Box>
        <Link className="main-button" to="/duration">
          Siguiente <Arrow fontSize="large"/>
        </Link>
      </Box>
    )
}

export { WeekDay }