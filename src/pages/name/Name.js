import { Button, Box, TextField } from "@mui/material"
import { database } from "../../firebase.config"
import { Link } from "react-router-dom"

import ChevronLeft from '@mui/icons-material/ChevronLeft';
import Arrow from '@mui/icons-material/ArrowForward';


function Name() {

  const setUserName = () => {
   const value = document.getElementsByTagName('input')[0].value
    database
    .ref("user")
    .set({
      name: value
    })
    .catch(alert)
    .then(() => {
      localStorage.setItem("user", value)
    })
  }

  return (
    <Box className="main-container">
        <Link className="return-button" variant="text" to="/" >
          <ChevronLeft fontSize="medium"/> <span>Volver</span>
        </Link>
        <Box>
          <p className="text-a">¿Cómo quieres que te llamemos?</p>
          <p className="text-b">Indica tu nombre o alias</p>
          <TextField variant="outlined" fullWidth/>
        </Box>
        <Link className="main-button" to="/week-day" onClick={setUserName}>
        Siguiente <Arrow fontSize="large"/>
      </Link>
    </Box>
    )
}

export { Name }
