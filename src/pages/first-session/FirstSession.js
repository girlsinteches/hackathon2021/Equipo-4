import { Link } from "react-router-dom"
import { Box, TextField } from "@mui/material"
import Arrow from '@mui/icons-material/ArrowForward';
import DatePicker from '@mui/lab/DatePicker';
import { LocalizationProvider } from "@mui/lab";
import AdapterMoment from '@mui/lab/AdapterMoment';
import ChevronLeft from '@mui/icons-material/ChevronLeft';

function FirstSession() {
  return (
    <Box className="main-container">
      <Box>
        <Link className="return-button" variant="text" to="/duration" >
          <ChevronLeft fontSize="medium"/> <span>Volver</span>
        </Link>
        <p className="text-a">¿Cuándo es la primera sesión?</p>
        <p>Indica la fecha de la primera sesión</p>
        <LocalizationProvider dateAdapter={AdapterMoment}>
          <DatePicker
            renderInput={(params) => <TextField fullWidth {...params} />}
          />
        </LocalizationProvider>
      </Box>
      <Link className="main-button" to="/finish">
        Siguiente <Arrow fontSize="large"/>
      </Link>
    </Box>
  )
}

export { FirstSession }