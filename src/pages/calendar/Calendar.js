import { Box } from "@mui/system";
import * as moment from "moment";
import {
  MonthlyBody,
  MonthlyCalendar,
  DefaultMonthlyEventItem,
  MonthlyDay,
} from "@zach.codes/react-calendar";
import Add from "@mui/icons-material/Add";

import { format, startOfMonth, subDays } from "date-fns";
import "@zach.codes/react-calendar/dist/calendar-tailwind.css";
import "./styles.css";
import { Link } from "react-router-dom";

export function Calendar() {
  const events = [
    { title: "Sesion 1", date: new Date() },
    { title: "Sesion 2", date: subDays(new Date(), 7) },
    { title: "Sesion 3", date: subDays(new Date(), 14) },
  ];
  return (
    <Box className="main-container">
      <div>
        <MonthlyCalendar
          currentMonth={startOfMonth(new Date())}
          onCurrentMonthChange={(date) => date}
        >
          <Link to="/duration">{"< volver"}</Link>
          <h1 className="text-a calendar-title">Calendario</h1>
          <MonthlyBody events={events}>
            <MonthlyDay
              renderDay={(data) =>
                data.map((item, index) => (
                  <DefaultMonthlyEventItem
                    key={index}
                    title={""}
                    // Format the date here to be in the format you prefer
                    date={""}
                  />
                ))
              }
            />
          </MonthlyBody>
        </MonthlyCalendar>
        <div class="events">
          {events.map((event, i) => (
            <div class="event-item">
              <div className="event-date">
                <p className="event-day">{moment(event.date).format("DD")}</p>
                <p className="event-hour">{moment(event.date).format("MMM")}</p>
              </div>
              <div className="event-info">
                <p className="event-name">Sesion {i + 1}</p>
                <p className="event-place">Hospital Valencia</p>
              </div>
            </div>
          ))}
        </div>
      </div>

      <button className="main-button finish-button">
        Agregar cita
        <Add fontSize="large" />
      </button>
    </Box>
  );
}
